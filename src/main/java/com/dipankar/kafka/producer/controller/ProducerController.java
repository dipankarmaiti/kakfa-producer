package com.dipankar.kafka.producer.controller;

import com.dipankar.kafka.producer.model.Order;
import com.dipankar.kafka.producer.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("kafka/publish")
public class ProducerController {

    @Autowired
    private ProducerService producerService;

    @PostMapping("/json")
    public String publishJsonMessage(@RequestBody Order order){
        return producerService.publishMessageAsJson(order);
    }

    @PostMapping("/key")
    public String publishMessageWithKey(@RequestBody Order order){
        return producerService.publishMessageWithKey(order);
    }

    @PostMapping("/string")
    public String publishStringMessage(@RequestBody Order order){
        return producerService.publishMessageAsString(order);
    }

    @PostMapping("/partition")
    public String logPartitionOffsetInfo(@RequestBody Order order){
        return producerService.publishMessageWithPartitionInfo(order);
    }


}
