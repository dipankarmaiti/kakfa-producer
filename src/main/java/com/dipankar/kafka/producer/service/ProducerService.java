package com.dipankar.kafka.producer.service;

import com.dipankar.kafka.producer.model.Order;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class ProducerService {
    @Value("${kafka.topic.order}")
    private String topicName;
    @Autowired
    private KafkaTemplate kafkaTemplate;
    @Autowired
    private ObjectMapper objectMapper;

    public String publishMessageAsJson(Order order){
        kafkaTemplate.send(topicName,order);
        return "Message published successfully";
    }
    public String publishMessageAsString(Order order) {
        try {
            String jsonString = objectMapper.writeValueAsString(order);
            kafkaTemplate.send(topicName, order);
            return "Message published successfully";
        }catch(Exception e){
            e.printStackTrace();
            return "Message could not be sent";
        }
    }
    public String publishMessageWithKey(Order order){
        kafkaTemplate.send(topicName,order.getOrderNo(),order);
        return "Message published successfully";
    }
    public String publishMessageWithPartitionInfo(Order order){
        try {
            CompletableFuture<SendResult<String, Object>> future = kafkaTemplate.send(topicName, order.getOrderNo(), order).toCompletableFuture();
            future.whenComplete((result, ex) -> {
                if (ex != null) {
                    System.err.println( "Failed to publish message:");
                    ex.printStackTrace();
                } else {
                    int partition = result.getRecordMetadata().partition();
                    long offset = result.getRecordMetadata().offset();
                    System.out.println("Published message to partition " + partition + ", offset " + offset);
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
            return "Message could not be published";
        }
        return "Message published successfully";
    }
}
